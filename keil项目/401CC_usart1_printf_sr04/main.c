#include <stdio.h>
#include "stm32f4xx.h"
#include "bit_band.h"

/*  .----------------.  .----------------.  .-----------------. .----------------.  .----------------.  .----------------.  .-----------------. .----------------. 
| .--------------. || .--------------. || .--------------. || .--------------. || .--------------. || .--------------. || .--------------. || .--------------. |
| |  ____  ____  | || |      __      | || | ____  _____  | || |    ______    | || |  ___  ____   | || |      __      | || | ____  _____  | || |    ______    | |
| | |_  _||_  _| | || |     /  \     | || ||_   \|_   _| | || |  .' ___  |   | || | |_  ||_  _|  | || |     /  \     | || ||_   \|_   _| | || |  .' ___  |   | |
| |   \ \  / /   | || |    / /\ \    | || |  |   \ | |   | || | / .'   \_|   | || |   | |_/ /    | || |    / /\ \    | || |  |   \ | |   | || | / .'   \_|   | |
| |    \ \/ /    | || |   / ____ \   | || |  | |\ \| |   | || | | |    ____  | || |   |  __'.    | || |   / ____ \   | || |  | |\ \| |   | || | | |    ____  | |
| |    _|  |_    | || | _/ /    \ \_ | || | _| |_\   |_  | || | \ `.___]  _| | || |  _| |  \ \_  | || | _/ /    \ \_ | || | _| |_\   |_  | || | \ `.___]  _| | |
| |   |______|   | || ||____|  |____|| || ||_____|\____| | || |  `._____.'   | || | |____||____| | || ||____|  |____|| || ||_____|\____| | || |  `._____.'   | |
| |              | || |              | || |              | || |              | || |              | || |              | || |              | || |              | |
| '--------------' || '--------------' || '--------------' || '--------------' || '--------------' || '--------------' || '--------------' || '--------------' |
 '----------------'  '----------------'  '----------------'  '----------------'  '----------------'  '----------------'  '----------------'  '----------------'  */

/* 
说明：
开箱即用代码-----------
注意！！！！！！！！！！！！！！！！！！！
1.如果你没有代码更改能力，请勿随意更改代码！  自己删注释，换函数名，换变量名，以免重复，建议更换控制部分的代码(A,B,C,D,...)
2.功能说明：
	1).前四个按键分别实现流水灯
	2).第五个按键实现采集一次距离传感器数据，并打印到虚拟终端上
	3).第六个按键实现采集一次距离温湿度数据，并打印到虚拟终端上
	4).蜂鸣器在温度超过30，距离超过100会报警，连续报警
	5).蓝牙串口控制
3.蓝牙串口控制说明：（控制部分代码可以自己尝试更改 841 行左右）
	1).连接串口发送’A‘,‘B','C','D'所实现功能与前4个按键相同
	2).发送’E'，‘e'分别控制距离传感器的开启与关闭
	3).发送’F'，‘f'分别控制距离传感器的开启与关闭


*/

//定义GPIO配置信息结构体
GPIO_InitTypeDef   GPIO_InitStructure;
//外部中断配置信息结构体
EXTI_InitTypeDef   EXTI_InitStructure;
//中断通道配置信息结构体
NVIC_InitTypeDef   NVIC_InitStructure;

//串口配置信息结构体
USART_InitTypeDef  USART_InitStructure;

int key5_flag=0,key6_flag=0,key7_flag=0,key8_flag=0,light_flag=0,ret=0,distanceCtrl=0,temCtrl=0,key14_flag=0,key15_flag=0;

unsigned char dht11_data[5];

//重定义fputc函数
int fputc(int ch, FILE *f)
{
	
	//把一个字符发送给串口1
	USART_SendData(USART6, ch);
	
	/* Loop until the end of transmission 判断发送是否完成*/
	while (USART_GetFlagStatus(USART6, USART_FLAG_TC) == RESET);

	
	//把一个字符发送给串口1
	//USART_SendData(USART6, ch);
	
	/* Loop until the end of transmission 判断发送是否完成*/
	//while (USART_GetFlagStatus(USART6, USART_FLAG_TC) == RESET);

	return ch;
}

//精准延时函数		
void delay_ms(int x)
{
	
	
	//延时50ms---》最大199.7ms
	//把计数值加载到LOAD寄存器 1ms == 84000
	SysTick->LOAD = 84000*x;
	SysTick->VAL = 0;	//清空计数值
	//让定时器开始工作
	SysTick->CTRL |= 0x1<<0;
	
	//判断Systick->LOAD是否减到0,它的CTRL第16为1
	//等于0时，它的CTRL还没减到0
	while( 0 == (SysTick->CTRL & (0x1<<16 )));
	SysTick->VAL = 0;	//清空计数值
	//关闭定时器
	SysTick->CTRL &= ~(0x1<<0);

}
//精准延时函数		
void delay_ms1(int x)
{
	int i;
	for(i=0; i<x/100; i++)
	{
		//延时50ms---》最大199.7ms
		//把计数值加载到LOAD寄存器 1ms == 84000
		SysTick->LOAD = 84000*100;
		SysTick->VAL = 0;	//清空计数值
		//让定时器开始工作
		SysTick->CTRL |= 0x1<<0;
		
		//判断Systick->LOAD是否减到0,它的CTRL第16为1
		//等于0时，它的CTRL还没减到0
		while( 0 == (SysTick->CTRL & (0x1<<16 )));
		SysTick->VAL = 0;	//清空计数值
		//关闭定时器
		SysTick->CTRL &= ~(0x1<<0);
	}
}

//精准延时函数		100
void delay_us(int x)
{
	
	
	//延时50ms
	//把计数值加载到LOAD寄存器 1ms == 10500 
	SysTick->LOAD = 84*x;
	SysTick->VAL = 0;	//清空计数值
	//让定时器开始工作
	SysTick->CTRL |= 0x1<<0;
	
	//判断Systick->LOAD是否减到0,它的CTRL第16为1
	//等于0时，它的CTRL还没减到0
	while( 0 == (SysTick->CTRL & (0x1<<16 )));
	SysTick->VAL = 0;	//清空计数值
	//关闭定时器
	SysTick->CTRL &= ~(0x1<<0);

}


//不精准延时
void delay()
{
	volatile int i, j;
	for(j=0; j<1000; j++)
		for(i=0; i<500; i++);

}

//串口1初始化--》PA9 --TX   PA10---RX
void usart1_init(uint32_t baud)
{
	/* 使能GPIOA的硬件时钟 */
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);

	/* GPIOA Configuration: PA9(USART1_TX) PA10(USART1_RX) 把PA9和PA10配置为第二功能模式*/
	GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_9 | GPIO_Pin_10  ;  //
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;				//复用功能，使用引脚的第二功能
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;			
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP ;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	/* Connect USART1_TX pins to PA9 将USART1_TX 引脚连到PA9*/
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource9, GPIO_AF_USART1);
	
	/* Connect USART1_RX pins to PA10 将USART1_RX 引脚连到PA10 */
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource10, GPIO_AF_USART1);

	/* Enable USART1 clock 使能串口1时钟*/
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);

	USART_InitStructure.USART_BaudRate = baud;					//设置波特率
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;			//8位数据位
	USART_InitStructure.USART_StopBits = USART_StopBits_1;				//1位停止位
	USART_InitStructure.USART_Parity = USART_Parity_No;				//无校验		
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None; //不需要流控制，使用modem模式的时候才实现流控制功能
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;					//使能串口支持发送和接收数据
	USART_Init(USART1, &USART_InitStructure);


	/* NVIC configuration */
	/* Configure the Priority Group to 2 bits */
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);							//设置优先级分组

	/* Enable the USARTx Interrupt */
	NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;						//串口1中断优先级通道
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;					//设置抢占优先级		
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;						//设置响应优先级
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;							//使能串口1中断优先级
	NVIC_Init(&NVIC_InitStructure);
	 
	/* Enable USART使能串口 */
	USART_Cmd(USART1, ENABLE);														//使能串口1中断
	

	/* Enable the Rx buffer empty interrupt清空串口缓冲区 */
	USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);							//清空接受缓冲区，使能RX
}

//串口2初始化--》PA11 --TX   PA12---RX
void usart6_init(uint32_t baud)
{
	/* 使能GPIOA的硬件时钟 */
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);

	/* GPIOA Configuration: PA9(USART1_TX) PA10(USART1_RX) 把PA9和PA10配置为第二功能模式*/
	GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_11 | GPIO_Pin_12  ;  //
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;				//复用功能，使用引脚的第二功能
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;			
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP ;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	/* Connect USART6_TX pins to PA11 将USART6_RX 引脚连到PA11*/
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource11, GPIO_AF_USART6);
	
	/* Connect USART6_RX pins to 2 将USART6_RX 引脚连到PA12 */
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource12, GPIO_AF_USART6);

	/* Enable USART6 clock 使能串口6时钟*/
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART6, ENABLE);

	USART_InitStructure.USART_BaudRate = baud;					//设置波特率
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;			//8位数据位
	USART_InitStructure.USART_StopBits = USART_StopBits_1;				//1位停止位
	USART_InitStructure.USART_Parity = USART_Parity_No;				//无校验		
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None; //不需要流控制，使用modem模式的时候才实现流控制功能
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;					//使能串口支持发送和接收数据
	USART_Init(USART6, &USART_InitStructure);


	/* NVIC configuration */
	/* Configure the Priority Group to 2 bits */
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);							//设置优先级分组

	/* Enable the USARTx Interrupt */
	NVIC_InitStructure.NVIC_IRQChannel = USART2_IRQn;						//串口1中断优先级通道
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;					//设置抢占优先级		
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;						//设置响应优先级
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;							//使能串口1中断优先级
	NVIC_Init(&NVIC_InitStructure);
	 
	/* Enable USART使能串口 */
	USART_Cmd(USART6, ENABLE);														//使能串口1中断
	

	/* Enable the Rx buffer empty interrupt清空串口缓冲区 */
	USART_ITConfig(USART6, USART_IT_RXNE, ENABLE);							//清空接受缓冲区，使能RX
}

//PA1~PA4引脚配置为输出模式
void LED_init()
{
	
	//初始化硬件时钟--》打开第A组GPIO硬件时钟
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);

	/* Configure PA0~PA3 in output pushpull mode */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_4 | GPIO_Pin_1 | GPIO_Pin_2 | GPIO_Pin_3 | GPIO_Pin_13;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;			//设置输出模式
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;			//设置推挽输出
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;		//设置运行速度100MHz
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;		//不需要上拉电阻
	
	//初始化第F组GPIO口		
	GPIO_Init(GPIOA, &GPIO_InitStructure);
	
	//关闭LED0灯
	PAout(4) = 1;
	PAout(1) = 1;
	PAout(2) = 1;
	PAout(3) = 1;
	
	// 蜂鸣器
	PAout(13) = 0;
	
	
}

// 配置PA9引脚为输入模式
void PA9_init(){

	GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_9 ;
	
	// GPIO 为输入模式
	GPIO_InitStructure.GPIO_Mode= GPIO_Mode_IN ;
	
	GPIO_InitStructure.GPIO_Speed= GPIO_Speed_100MHz;
	
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	
	GPIO_Init(GPIOA, &GPIO_InitStructure);
}

//TRIG--PB14， echo--PB15
void set_sr04_port_mode()
{
	
	//初始化硬件时钟--》打开第A组GPIO硬件时钟
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);

	/* Configure PA0~PA3 in output pushpull mode */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_14;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;			//设置输出模式
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;			//设置推挽输出
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;		//设置运行速度100MHz
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;		//不需要上拉电阻
	
	//初始化第F组GPIO口		
	GPIO_Init(GPIOB, &GPIO_InitStructure);
	
	//初始化硬件时钟--》打开第A组GPIO硬件时钟
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
	
	/* Configure PA0 in input pushpull mode */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_15;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;			//设置输入模式
	//GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;			//设置推挽输出
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;		//设置运行速度100MHz
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;		//不需要上拉电阻
	
	//初始化第A组GPIO口		
	GPIO_Init(GPIOB, &GPIO_InitStructure);

}

//获取超声波的距离
unsigned int sr04_get_distance()
{
	unsigned int count=0;
	PBout(14) = 1;
	delay_us(20);
	
	PBout(14) = 0;
	
	//等待echo引脚地点时间持续结束
	while(0 == PBin(15));
	
	//计算有多少个三毫米，计算定时多少次
	while(1 == PBin(15))
	{
		count++;
		delay_us(9);
	
	}
	
	//返回距离mm
	return count*3/2;

}

//配置PA5引脚的外部中断
void exti5_init()
{
	/* Enable GPIOA clock 使能第A组的硬件时钟*/
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);

	/* Enable SYSCFG clock 使能系统时钟*/
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);
	
	/* Configure PA5 pin as input floating 把PA5配置为输入模式*/
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;		//输出入模式
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;	//不需要上拉电阻
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_5;		//选择0号引脚
	GPIO_Init(GPIOA, &GPIO_InitStructure);			//初始化GPIO
	
	/* Connect EXTI Line5 to PA5 pin 把PA4连接到外部中断控制线0*/
	SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOA, EXTI_PinSource5);
	
	/* Configure EXTI Line5 配置外部中断控制线5*/
	EXTI_InitStructure.EXTI_Line = EXTI_Line5;			//选择外部中断线0
	EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;		//选择中断模式--》外部中断
	EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling;  	//选择下降沿触发
	EXTI_InitStructure.EXTI_LineCmd = ENABLE;			//使能
	EXTI_Init(&EXTI_InitStructure);			//初始化
	//设置中断优先级分组抢占优先级和响应优先级各 2bit
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
	
	/* Enable and set EXTI Line5 Interrupt to the lowest priority中断通道优先级设置 */
	NVIC_InitStructure.NVIC_IRQChannel = EXTI9_5_IRQn;		//选择中断通道
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x00;	//设置抢占优先级为:0x01
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x01;		//设置响应优先级为：0x01
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;			//使能
	NVIC_Init(&NVIC_InitStructure);					//初始化

}

//配置PA6引脚的外部中断
void exti6_init()
{
	/* Enable GPIOA clock 使能第A组的硬件时钟*/
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);

	/* Enable SYSCFG clock 使能系统时钟*/
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);
	
	/* Configure PA5 pin as input floating 把PA5配置为输入模式*/
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;		//输出入模式
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;	//不需要上拉电阻
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6;		//选择0号引脚
	GPIO_Init(GPIOA, &GPIO_InitStructure);			//初始化GPIO
	
	/* Connect EXTI Line5 to PA5 pin 把PA4连接到外部中断控制线0*/
	SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOA, EXTI_PinSource6);
	
	/* Configure EXTI Line5 配置外部中断控制线5*/
	EXTI_InitStructure.EXTI_Line = EXTI_Line6;			//选择外部中断线0
	EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;		//选择中断模式--》外部中断
	EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling;  	//选择上升沿触发
	EXTI_InitStructure.EXTI_LineCmd = ENABLE;			//使能
	EXTI_Init(&EXTI_InitStructure);			//初始化
	//设置中断优先级分组抢占优先级和响应优先级各 2bit
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
	
	/* Enable and set EXTI Line5 Interrupt to the lowest priority中断通道优先级设置 */
	NVIC_InitStructure.NVIC_IRQChannel = EXTI9_5_IRQn;		//选择中断通道
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x00;	//设置抢占优先级为:0x01
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x02;		//设置响应优先级为：0x01
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;			//使能
	NVIC_Init(&NVIC_InitStructure);					//初始化

}

//配置PA7引脚的外部中断
void exti7_init()
{
	/* Enable GPIOA clock 使能第A组的硬件时钟*/
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);

	/* Enable SYSCFG clock 使能系统时钟*/
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);
	
	/* Configure PA5 pin as input floating 把PA5配置为输入模式*/
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;		//输出入模式
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;	//不需要上拉电阻
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_7;		//选择0号引脚
	GPIO_Init(GPIOA, &GPIO_InitStructure);			//初始化GPIO
	
	/* Connect EXTI Line5 to PA5 pin 把PA4连接到外部中断控制线0*/
	SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOA, EXTI_PinSource7);
	
	/* Configure EXTI Line5 配置外部中断控制线5*/
	EXTI_InitStructure.EXTI_Line = EXTI_Line7;			//选择外部中断线0
	EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;		//选择中断模式--》外部中断
	EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling;  	//选择下降沿触发
	EXTI_InitStructure.EXTI_LineCmd = ENABLE;			//使能
	EXTI_Init(&EXTI_InitStructure);			//初始化
	//设置中断优先级分组抢占优先级和响应优先级各 2bit
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
	
	/* Enable and set EXTI Line5 Interrupt to the lowest priority中断通道优先级设置 */
	NVIC_InitStructure.NVIC_IRQChannel = EXTI9_5_IRQn;		//选择中断通道
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x00;	//设置抢占优先级为:0x02
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x03;		//设置响应优先级为：0x02
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;			//使能
	NVIC_Init(&NVIC_InitStructure);					//初始化

}

//配置PA8引脚的外部中断
void exti8_init()
{
	/* Enable GPIOA clock 使能第A组的硬件时钟*/
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);

	/* Enable SYSCFG clock 使能系统时钟*/
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);
	
	/* Configure PA5 pin as input floating 把PA5配置为输入模式*/
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;		//输出入模式
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;	//不需要上拉电阻
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_8;		//选择0号引脚
	GPIO_Init(GPIOA, &GPIO_InitStructure);			//初始化GPIO
	
	/* Connect EXTI Line5 to PA5 pin 把PA4连接到外部中断控制线0*/
	SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOA, EXTI_PinSource8);
	
	/* Configure EXTI Line5 配置外部中断控制线5*/
	EXTI_InitStructure.EXTI_Line = EXTI_Line8;			//选择外部中断线0
	EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;		//选择中断模式--》外部中断
	EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling;  	//选择下降沿沿触发
	EXTI_InitStructure.EXTI_LineCmd = ENABLE;			//使能
	EXTI_Init(&EXTI_InitStructure);			//初始化
	//设置中断优先级分组抢占优先级和响应优先级各 2bit
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
	
	/* Enable and set EXTI Line5 Interrupt to the lowest priority中断通道优先级设置 */
	NVIC_InitStructure.NVIC_IRQChannel = EXTI9_5_IRQn;		//选择中断通道
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x00;	//设置抢占优先级为:0x01
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x04;		//设置响应优先级为：0x01
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;			//使能
	NVIC_Init(&NVIC_InitStructure);					//初始化

}
//配置PA15引脚的外部中断
void exti15_init()
{
	/* Enable GPIOA clock 使能第A组的硬件时钟*/
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);

	/* Enable SYSCFG clock 使能系统时钟*/
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);
	
	/* Configure PA5 pin as input floating 把PA5配置为输入模式*/
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;		//输出入模式
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;	//不需要上拉电阻
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_15;		//选择0号引脚
	GPIO_Init(GPIOA, &GPIO_InitStructure);			//初始化GPIO
	
	/* Connect EXTI Line5 to PA5 pin 把PA4连接到外部中断控制线0*/
	SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOA, EXTI_PinSource15);
	
	/* Configure EXTI Line5 配置外部中断控制线5*/
	EXTI_InitStructure.EXTI_Line = EXTI_Line15;			//选择外部中断线0
	EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;		//选择中断模式--》外部中断
	EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling;  	//选择下降沿沿触发
	EXTI_InitStructure.EXTI_LineCmd = ENABLE;			//使能
	EXTI_Init(&EXTI_InitStructure);			//初始化
	//设置中断优先级分组抢占优先级和响应优先级各 2bit
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
	
	/* Enable and set EXTI Line5 Interrupt to the lowest priority中断通道优先级设置 */
	NVIC_InitStructure.NVIC_IRQChannel = EXTI15_10_IRQn;		//选择中断通道
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x00;	//设置抢占优先级为:0x01
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x04;		//设置响应优先级为：0x01
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;			//使能
	NVIC_Init(&NVIC_InitStructure);					//初始化

}
//配置PA14引脚的外部中断
void exti14_init()
{
	/* Enable GPIOA clock 使能第A组的硬件时钟*/
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);

	/* Enable SYSCFG clock 使能系统时钟*/
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);
	
	/* Configure PA5 pin as input floating 把PA5配置为输入模式*/
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;		//输出入模式
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;	//不需要上拉电阻
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_14;		//选择0号引脚
	GPIO_Init(GPIOA, &GPIO_InitStructure);			//初始化GPIO
	
	/* Connect EXTI Line5 to PA5 pin 把PA4连接到外部中断控制线0*/
	SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOA, EXTI_PinSource14);
	
	/* Configure EXTI Line5 配置外部中断控制线5*/
	EXTI_InitStructure.EXTI_Line = EXTI_Line14;			//选择外部中断线0
	EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;		//选择中断模式--》外部中断
	EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling;  	//选择下降沿沿触发
	EXTI_InitStructure.EXTI_LineCmd = ENABLE;			//使能
	EXTI_Init(&EXTI_InitStructure);			//初始化
	//设置中断优先级分组抢占优先级和响应优先级各 2bit
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
	
	/* Enable and set EXTI Line5 Interrupt to the lowest priority中断通道优先级设置 */
	NVIC_InitStructure.NVIC_IRQChannel = EXTI15_10_IRQn;		//选择中断通道
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x00;	//设置抢占优先级为:0x01
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x04;		//设置响应优先级为：0x01
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;			//使能
	NVIC_Init(&NVIC_InitStructure);					//初始化

}

void set_PB12_output_mode()
{
	//初始化硬件时钟--》打开第A组GPIO硬件时钟
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);

	/* Configure PA0~PA3 in output pushpull mode */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_12;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;			//设置输出模式
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;			//设置推挽输出
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;		//设置运行速度100MHz
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;		//不需要上拉电阻
	
	//初始化第F组GPIO口		
	GPIO_Init(GPIOB, &GPIO_InitStructure);

}
void set_PB12_input_mode()
{
		//初始化硬件时钟--》打开第A组GPIO硬件时钟
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);

	/* Configure PA0~PA3 in output pushpull mode */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_12;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;			//设置输出模式
	//GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;			//设置推挽输出
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;		//设置运行速度100MHz
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;		//不需要上拉电阻
	
	//初始化第F组GPIO口		
	GPIO_Init(GPIOB, &GPIO_InitStructure);
	
	
}
//请求DHT11数据
int ask_dht11_data()
{
	int i=0;
	set_PB12_output_mode();
	PBout(12) = 0;
	delay_ms(20);

	PBout(12) = 1;
	delay_us(30);
	
	set_PB12_input_mode();
	
	//超时判断，判断100us内，PB12是否为低电平
	while(i<100)
	{
		if( 0 == PBin(12))
			break;
		i++;
		delay_us(1);
		
	}
	if(i>= 100)
		return -1;
	
	
	i=0;
	//判断低电平的持续时间在100us内
	while(i<100)
	{
		if( 1 == PBin(12))
			break;
		i++;
		delay_us(1);
		
	}
	if(i>= 100)
		return -1;
	
	return 1;
}

//读取1个字节数据--》8bit
unsigned char dht11_sent_1Byte_data()
{
	int i;
	unsigned char data=0;
	//等待dht11回应的高电平时间持续结束
	while(1 == PBin(12));
	
	for(i=0; i<8; i++)
	{
		//等待数据位的低电平持续结束
		while(0 == PBin(12));
		delay_us(40);
		//如果40us后，该引脚仍然为高电平
		if(1 == PBin(12))
			data |= 1<<(7-i); //把数据移动对应位，数据返回时，它从高位开始返回
		
		//等待数据位1的高电平时间持续结束
		while(1 == PBin(12));
	
	}
	
	return data;
	
}

//获取一次dht11完整数据
 int get_dhtll_all_data()
{
	int i;
	//请求DHT11返回数据
	while(-1 == ask_dht11_data());

	for(i=0; i<5; i++)
	{
		dht11_data[i] = dht11_sent_1Byte_data();
	}
	//计算校验和是否相等
	if(dht11_data[4] == dht11_data[0] + dht11_data[1] + dht11_data[2] + dht11_data[3])
		return 1;
	else
		return -1;
}

int main()
{
	int distance = 0;
	int tem = 0;
	int hum = 0;
	int i=1,j=0,k=0;
	LED_init();
	exti5_init();
	exti6_init();
	exti7_init();
	exti8_init();
	exti14_init();
	exti15_init();
	
	PA9_init();
	
	//初识化系统时钟--》打开系统时钟，设置频率84MHz
	SysTick_CLKSourceConfig(SysTick_CLKSource_HCLK);
	
	usart1_init(9600);
	usart6_init(9600);
	set_sr04_port_mode();


	printf("hello world!\r\n" );
	
	//exti0_init();

	while(1) {//空循环
	

	
	if(key15_flag==1){
	// 进行温湿度采集模块的控制
		//获取温湿度模块数据
		if(1== get_dhtll_all_data()){
		delay_ms1(300);
		printf("H:%d.%d T:%d.%d\r\n", dht11_data[0], dht11_data[1], dht11_data[2],dht11_data[3]);
		if(dht11_data[2]>30){
			PAout(13) = 1;
			}else{
			PAout(13) = 0;
			}
		}
		delay_ms(300);

	}	
		
	if( key14_flag==1){
		
		distance = sr04_get_distance();
		
		delay_ms1(300);
		printf("dis: %d\r\n",distance);
		
		if(distance>1000){
			PAout(13) = 1;
		}else{
			PAout(13) = 0;
		}

		
	}	
	if(key5_flag == 1)
	{
		key5_flag=0;
		//D1亮
		PAout(1)=0;
	}
	
	if(key6_flag ==1)
	{
		key6_flag=0;
		//关闭LED0灯
		PAout(4) = 1;
		PAout(1) = 1;
		PAout(2) = 1;
		PAout(3) = 1;
	}
	if(key7_flag ==1)
	{
		
		key7_flag=0;		
		i=light_flag;
		
		i=i%5;
		while((!(key5_flag || key6_flag || key8_flag ))){
		//正向的循环流水灯
		PAout(i) = 0;
		delay();
		
		PAout(i) = 1;
		delay();
		light_flag=i;
		i++;
		i=i%5;
	
	}	
	}
	if(key8_flag == 1)
	{
		delay_ms(20);
		key8_flag=0;
		
		ret++;
		//》切换不断循环的流水灯的方向：
		PAout(4) = 1;
		PAout(1) = 1;
		PAout(2) = 1;
		PAout(3) = 1;
		PAout(4) = 1;
		if(ret %2 == 1 )
		{
			//正向的循环流水灯
			
			j=light_flag;
			while((!(key5_flag || key6_flag || key8_flag ))){
				//正向的循环流水灯
				PAout((5-j)) = 0;
				delay();
				PAout((5-j)) = 1;
				delay();
				light_flag=j;
				j++;
				j=j%5;
			}	
		}
		if(ret %2 == 0)
		{
			
			//反向流水灯
			
			k=light_flag;
			while((!(key5_flag || key6_flag || key8_flag ))){
				//正向的循环流水灯
				PAout(k) = 0;
				delay();
				PAout(k) = 1;
				delay();
				light_flag=k;
				k++;
				k=k%5;
			}
		}

	}

	
}
	
}

//每接受到一个字节产生一次中断
void USART1_IRQHandler(void)
{
	uint8_t d;

	/* USART in Receiver mode 串口接收到数据的标志位*/
	if (USART_GetITStatus(USART1, USART_IT_RXNE) == SET)  
	{
		
		d= USART_ReceiveData(USART1);
		if(d=='A'){
			key5_flag=1;
		}else if(d=='B'){
			key6_flag=1;
		}else if(d=='C'){
			key7_flag=1;
		}else if(d=='D'){
			key8_flag=1;
		}else if(d=='E'){
			key14_flag=1;
		}else if(d=='e'){
			key14_flag=0;
		}else if(d=='F'){
			key15_flag=1;
		}else if(d=='f'){
			key15_flag=0;
		}
		
		
		/* Loop until the end of transmission */
		while (USART_GetFlagStatus(USART1, USART_FLAG_TC) == RESET);

	}





}

////外部中断控制线5~9的中断处理函数
void EXTI9_5_IRQHandler(void)
{
	
			//外部中断控制线4是否真的处理
	if(EXTI_GetITStatus(EXTI_Line7) != RESET)
	{
		

		key7_flag=1;
		
		
		EXTI_ClearITPendingBit(EXTI_Line7);
	}else
	
		
	if(EXTI_GetITStatus(EXTI_Line8) != RESET)
	{

		key8_flag=1;		
		
		
		EXTI_ClearITPendingBit(EXTI_Line8);
	}else
	//外部中断控制线4是否真的处理
	if(EXTI_GetITStatus(EXTI_Line5) != RESET)
	{
		
		key5_flag=1;
		
		
		//清除外部中断控制线4的标志位
		/* Clear the EXTI line 5 pending bit */
		EXTI_ClearITPendingBit(EXTI_Line5);
	}else
	
		
	if(EXTI_GetITStatus(EXTI_Line6) != RESET)
	{
		key6_flag=1;
		
		
		
		EXTI_ClearITPendingBit(EXTI_Line6);
	}
	

	
	
}

////外部中断控制线10~15的中断处理函数
void EXTI15_10_IRQHandler(void)
{
	
			//外部中断控制线4是否真的处理
	if(EXTI_GetITStatus(EXTI_Line14) != RESET)
	{
		

		key14_flag=1;
		
		
		EXTI_ClearITPendingBit(EXTI_Line14);
	}else
	
		
	if(EXTI_GetITStatus(EXTI_Line15) != RESET)
	{

		key15_flag=1;		
		
		
		EXTI_ClearITPendingBit(EXTI_Line15);
	}
}





